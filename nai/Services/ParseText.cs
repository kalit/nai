﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using Nai.ViewModels;
using Tesseract;

namespace Nai.Services
{
    public class ParseText : IDisposable
    {
        private readonly Regex _urlRegex;
        private readonly TesseractEngine _engine;

        public ParseText()
        {
            _urlRegex = new Regex(".+(http:\\/\\/.+\\.pl)");
            _engine = new TesseractEngine(@"C:\dev\nai\nai\tessdata", "eng", EngineMode.Default);
        }

        public IEnumerable<TextFromCamera> ParseTextFromImage(Bitmap image)
        {
            using (var page = _engine.Process(image))
            {
                var url = page.GetText();

                if (_urlRegex.IsMatch(url))
                {
                    var @group = _urlRegex.Match(url).Groups[0];
                    foreach (var c in @group.Captures)
                    {
                        yield return new TextFromCamera
                        {
                            Text = c.ToString()
                        };
                    }
                }
            }
        }

        public void Dispose()
        {
            _engine?.Dispose();
        }
    }
}