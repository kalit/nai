using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using AForge.Imaging.Filters;
using AForge.Video;
using AForge.Video.DirectShow;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Microsoft.Win32;
using Nai.Services;

namespace Nai.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private readonly ParseText _parseText;
        private VideoCaptureDevice _videoCaptureDevice;
        private DispatcherTimer _timer;
        private Bitmap _image;
        private BitmapSource _cameraFrame;
        private ICollection<TextFromCamera> _textFromCamera;

        public MainViewModel(ParseText parseText)
        {
            _parseText = parseText;
            _textFromCamera = new List<TextFromCamera>();
        }

        public ICommand LoadServices => new RelayCommand(LoadServiceExecute);

        public ICommand DisposeServices => new RelayCommand(DisposeServiceExecute);

        public BitmapSource CameraFrame
        {
            get
            {
                return _cameraFrame;
            }
            set
            {
                _cameraFrame = value;
                RaisePropertyChanged(() => CameraFrame);
            }
        }

        public IEnumerable<TextFromCamera> TextFromCamera
        {
            get
            {
                return _textFromCamera.ToList();
            }
            set
            {
                foreach (var var in value)
                {
                    _textFromCamera.Add(var);
                }

                RaisePropertyChanged(() => TextFromCamera);
            }
        }

        private void LoadServiceExecute()
        {
            _videoCaptureDevice = new VideoCaptureDevice(new FilterInfoCollection(FilterCategory.VideoInputDevice)[0].MonikerString);
            _videoCaptureDevice.NewFrame += CamNewFrame;

            _videoCaptureDevice.Start();
            ParseText();
        }

        private void DisposeServiceExecute()
        {
            _videoCaptureDevice.SignalToStop();
            _timer.Stop();
            _parseText.Dispose();
        }

        private void CamNewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            try
            {
                var imageForCapture = (Bitmap)eventArgs.Frame.Clone();

                var image = ParseImage(imageForCapture);

                Image img = image;
                MemoryStream ms = new MemoryStream();
                img.Save(ms, ImageFormat.Bmp);
                ms.Seek(0, SeekOrigin.Begin);
                var bi = new BitmapImage();
                bi.BeginInit();
                bi.StreamSource = ms;
                bi.EndInit();

                bi.Freeze();
                Application.Current.Dispatcher.Invoke(new ThreadStart(delegate
                {
                    CameraFrame = bi;
                }));
            }
            catch (Exception)
            {
                // ignored
            }
        }

        private Bitmap ParseImage(Bitmap image)
        {
            Grayscale filterToGrayscale = new Grayscale(0.5125, 0.7154, 0.0721);
            var grayscaleImage = filterToGrayscale.Apply(image);
            var filter = new BradleyLocalThresholding();
            filter.ApplyInPlace(grayscaleImage);

            _image = grayscaleImage;

            return grayscaleImage;
        }

        private void ParseText()
        {
            _timer = new DispatcherTimer
            {
                Interval = TimeSpan.FromMilliseconds(1000)
            };

            _timer.Tick += (s, @event) =>
            {
                if (_image == null)
                {
                    return;
                }

                try
                {
                    var textFromImage = _parseText.ParseTextFromImage(new Bitmap(_image));
                    TextFromCamera = textFromImage;
                }
                catch (Exception)
                {
                    // ignored
                }
            };

            _timer.Start();
        }
    }

    public class TextFromCamera
    {
        public string Text { get; set; }

        public ICommand GoToPage => new RelayCommand<string>(url => GoToPageExecute(url));

        private void GoToPageExecute(string url)
        {
            OpenWebsite(url);
        }

        private static void OpenWebsite(string url)
        {
            Process.Start(GetDefaultBrowserPath(), url);
        }

        private static string GetDefaultBrowserPath()
        {
            string key = @"http\shell\open\command";
            RegistryKey registryKey =
            Registry.ClassesRoot.OpenSubKey(key, false);
            return ((string)registryKey.GetValue(null, null)).Split('"')[1];
        }
    }
}